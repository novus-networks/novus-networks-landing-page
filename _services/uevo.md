---
title: "Uevo: Innovation Analytics"
date: 2018-11-18T12:33:46+10:00
featured: true
weight: 1
---

A web platform to consolidate, visualize and distribute data-driven innovation insights sourced directly from patent databases at an affordable price for companies.

The platform will enable companies to:
- Map Out Areas of Opportunity
- Predict Innovation Trajectories

## Map Out Areas of Opportunity

![NovusMap](\images\screenshots\novusmap_home.png)

Our curated data visualizations give you a broad survey of the innovation landscape to give both your team and your stakeholders a current and complete picture of the state of your competitors, your sector and other related sectors.

### Your Sectors

![NovusMap_1](\images\screenshots\novusmap_1.png)

We show you what your sectors’ innovation landscapes look like.

### Your Competitors

![NovusMap_2](\images\screenshots\novusmap_2.png)

We show you what your competitors are doing.

### Emerging Tech

![NovusMap_2](\images\screenshots\novusmap_3.png)

We show you what’s new and emerging in your specific sector.

## Predict Innovation Trajectories

![NovusPredict](\images\screenshots\novuspredict_home.png)

Based on well-established research on technological relatedness and patent classification systems, our prediction algorithms aim to guide your strategic foresight into applicable innovation opportunities, saving you time and money as opposed to developing solutions from scratch.

### Curated to Your Sector

![NovusPredict_1](\images/screenshots/novuspredict_1.png)

We predict and curate the most viable technologies for recombinant innovation tailored to your specific sector, based on a vast portfolio of combinations that have worked in your industry and beyond.

### Evaluation Metrics

![NovusPredict_2](\images/screenshots/novuspredict_2.png)

We design relevant evaluation metrics which tell you how applicable and novel a predicted technology is, to let you and your team decide on the best innovation trajectory to take.

### Up to 85% Accuracy

![NovusPredict_3](\images\screenshots\novuspredict_3.png)

We continuously test and refine our models to provide you with the most accurate technology forecasts. To date, our models have been tested on more than 50 years of patent data, achieving an accuracy of up to 85% for a prediction period of 5 to 7 years.
