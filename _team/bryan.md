---
title: "Bryan Ong"
date: 2018-11-19T10:47:58+10:00
draft: false
image: "images/team/bryan.jpg"
jobtitle: "Co-Founder"
promoted: true
linkedinurl: "https://www.linkedin.com/in/bryan-ong-wen-xi-b65769146/"
weight: 1
---

Bryan is a graduate student at MIT pursuing civil and environmental engineering and building technology.
