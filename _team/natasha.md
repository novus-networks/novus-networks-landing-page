---
title: 'Natasha'
date: 2018-12-20T13:44:30+10:00
draft: false
image: 'images/team/natasha.jpg'
promoted: true
jobtitle: 'Co-Founder'
linkedinurl: "https://www.linkedin.com/in/natasha-ann-mei-seem-lum-969703186"
weight: 2
---

Natasha is a graduate student in Boston University studying computer information systems and innovation.
