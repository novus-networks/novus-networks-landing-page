---
layout: home
title: Novus Networks
description: A software company specializing in developing data-driven tools to help companies stay ahead of evolving business strategies.
intro_image: "images/screenshots/novusmap_home.png"
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# Novus Networks

A software company specializing in developing data-driven tools to help companies stay ahead of evolving business strategies.
