---
title: Services
layout: services
intro_image: "images/illustrations/Explore.png"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# Services that grow with your business

At Novus Networks, we specialize in developing data-driven tools to help companies stay ahead of these evolving business needs in an affordable and sustainable manner.

We currently have two tools:

## Uevo
A web platform to consolidate, visualize and distribute data-driven innovation insights sourced directly from patent databases at an affordable price for companies.

## Onit
An automated social media marketing service that helps you build your online presence in a sustainable and organic fashion relevant to your needs.
